# Fortran - Pendulo elastico - Versão G2

Simula a oscilação de um pêndulo preso a um elástico sob ação da gravidade, utilizando a biblioteca gráfica G2. 

As equações do movimento são:

d²x/dt² = -(k/m)( 1 - l0/l )( x - x0 )

d²y/dt² = -g - (k/m)( 1 - l0/l )( y - y0 )

g é a aceleração da gravidade
k é o coeficiente elástico
m a massa do peso na ponta do pêndulo
l0 é comprimento de relaxamento da mola/elástico
l é o comprimento da mola/elástico e é dado por l = sqrt( (x-x0)² + (y-y0)² )
(x0,y0) é a coordenada do extremo fixo do pêndulo
(x,y) é a coordenada do peso no outro extremo

O método numérico utilizado para resolver as equações de movimento são o método da diferença centrada

f'(x) = ( f(x+h) - f(x-h) )/2h

e a fórmula da diferença centrada para a segunda derivada

f''(x) = ( f(x+h) - 2f(x) + f(x-h) )/h²
