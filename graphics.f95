    MODULE graphics

        ! Tamanho da tela.
        real width, height
        parameter(width=800.0)
        parameter(height=600.0)

        CONTAINS

        ! Inicializa os devices do g2.
        SUBROUTINE initGraph(virtualDevice,x11Device)

            real virtualDevice, x11Device

            virtualDevice = g2_open_vd_()
            x11Device = g2_open_x11_(width,height)

            call g2_attach_(virtualDevice,x11Device)

        END SUBROUTINE initGraph

        ! Imprime as informações de velocidade do pêndulo.
        SUBROUTINE drawInfo(device,x,y,string)

            real device,x,y
            character string

            call g2_pen_(device,1.0)
            call g2_string_(x11Device,50.0,50.0,string)
            call g2_flush_(device)

        END SUBROUTINE drawInfo

        ! Desenha o pêndulo na posição calculada.
        SUBROUTINE drawPendule(device,x0,y0,x1,y1)

            real device,x0,y0,x1,y1

            call g2_pen_(device,1.0)
            call g2_line_( device,x0,y0,x1,y1)
            call g2_pen_(device,7.0)
            call g2_filled_circle_(device,x1,y1,10.0)
            call g2_flush_(device)

        END SUBROUTINE drawPendule

        ! Seta como branco o pêndulo anterior mesclando com o fundo, simulando que apagou.
        SUBROUTINE erasePendule(device,x0,y0,x1,y1)

            real device,x0,y0,x1,y1

            call g2_pen_(device,0.0)
            call g2_line_( device,x0,y0,x1,y1)
            call g2_filled_circle_(device,x1,y1,10.0)

        END SUBROUTINE erasePendule

        ! Fecha os devices.
        SUBROUTINE closeGraph(virtualDevice,x11Device)

            implicit none

            real virtualDevice, x11Device

            call g2_close_(x11Device)
            call g2_close_(virtualDevice)

        END SUBROUTINE closeGraph

    END MODULE graphics
