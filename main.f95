  PROGRAM pendulo_elastico

    use library
    use graphics

    real h
    parameter(h=1e-03)

    integer t
    real x(3),y(3)
    real Vx,Vy

    real virtualDevice, x11Device

    call initGraph(virtualDevice,x11Device)

    x(1) = ( 400.0 - 100.0 )
    y(1) = 600.0
    call drawPendule(x11Device,x0,y0,x(1),y(1))
    call erasePendule(x11Device,x0,y0,x(1),y(1))

    ! Para todo t < 0, a velocidade do pêndulo era 0.
    ! Isso significa que a posição do pêndulo é x0 e y0 para
    ! todo t < 0.
    x(2) = x(1)
    y(2) = y(1)
    call drawPendule(x11Device,x0,y0,x(2),y(2))
    call erasePendule(x11Device,x0,y0,x(1),y(1))

    Vx = ( x(3) - x(2) )/(2*h)
    Vy = ( y(3) - y(2) )/(2*h)
    print*, "velocidade = ", V(Vx,Vy)/100.0 , "m/s"

    do while(.true.)

        x(3) = 2*x(2) - x(1) + (h**2)*Fx( x(1), y(1) )
        y(3) = 2*y(2) - y(1) + (h**2)*Fy( x(1), y(1) )

        Vx = ( x(3) - x(2) )/(2*h)
        Vy = ( y(3) - y(2) )/(2*h)
        print*, "velocidade = ", V(Vx,Vy)/100.0 , "m/s"

        call erasePendule(x11Device,x0,y0,x(2),y(2))
        call drawPendule(x11Device,x0,y0,x(3),y(3))

        x(1) = x(2)
        y(1) = y(2)

        x(2) = x(3)
        y(2) = y(3)

        call sleep(0)

    enddo

    call closeGraph(virtualDevice,x11Device)

  END PROGRAM pendulo_elastico
