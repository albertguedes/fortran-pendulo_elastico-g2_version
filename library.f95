    MODULE library

        implicit none

        ! Posição do eixo fixo do pêndulo. ( cm )
        real x0,y0
        parameter(x0=400.0)
        parameter(y0=600.0)

        ! Constante elástica do pêndulo. ( g*cm/s**2 )
        real k
        parameter(k=100)

        ! Massa do peso do pêndulo. ( g )
        real m
        parameter(m=20.0)

        ! Aceleração da gravidade. ( cm/s**2 )
        real g
        parameter(g=980.0)

        ! Comprimento de relaxamento da corda. ( cm )
        real l0
        parameter(l0=30.0)

        CONTAINS

        ! Retorna o módulo da velocidade.
        real FUNCTION V(Vx,Vy) RESULT(z)

            real Vx,Vy

            z = sqrt( Vx**2 + Vy**2 )

            return

        END FUNCTION V

        ! Força total na direção x exercido no pêndulo.
        real FUNCTION Fx(x,y) RESULT(z)

            real x,y,l

            l = sqrt( (x-x0)**2 + (y-y0)**2 )

            if( l .ge. l0 )then
                z =  -(k/m)*( 1 - l0/l )*( x - x0 )
            else
                z = 0.0
            endif

            return

        END FUNCTION Fx

        ! Força total na direção y exercido no pêndulo.
        real FUNCTION Fy(x,y) RESULT(z)

            real x,y,l

            l = sqrt( (x-x0)**2 + (y-y0)**2 )

            if( l .ge. l0 )then
                z =  -g -(k/m)*( 1 - l0/l )*( y - y0 )
            else
                z = -g
            endif

            return

        END FUNCTION Fy

    END MODULE library
