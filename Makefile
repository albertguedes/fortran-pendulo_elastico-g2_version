CFLAGS = -lg2 -lX11

main: graphics.o library.o main.o
	gfortran $(CFLAGS) -o main graphics.o library.o main.o

main.o: main.f95
	gfortran -c -o main.o main.f95

library.o: library.f95
	gfortran -c -o library.o library.f95

graphics.o: graphics.f95
	gfortran -c -o graphics.o graphics.f95
	
clean:
	rm main *~ *.o *.mod
